# Docker Laravel Quickstart

## Installation
```bash
# Clone Repository
git clone git@gitlab.com:matthewhoener/docker-laravel.git

# Composer install
cd docker-laravel
docker run --rm --interactive --tty \
  --volume $PWD:/app \
  --user $(id -u):$(id -g) \
  composer install

# Copy env file
cp .env.example .env

# Generate app key
docker run --rm \
  --volume $PWD:/var/www/html \
  php:7.4-fpm \
  php artisan key:generate

#  Install javascript dependencies
docker run --rm -it -v $HOME/.npm:/.npm -v $PWD:/app -w /app -u $(id -u):$(id -g) node:14 npm install

# Create Test Database (must be run while application is running)
docker exec -it db mysql -uroot -p'warning this password is under version control' -e 'create database laravel_test'
```

## Starting and stopping
```bash
# Start application
docker-compose up

# Stop application
docker-compose stop

# Destroy application 
docker-compose down
```

## Development
```bash
# Compile assets
docker run --rm -it -v $PWD:/app -w /app -u $(id -u):$(id -g) node:14 npm run dev

# Migrate DB
docker exec -it docker-laravel php artisan migrate

# Run tests
docker-compose exec app ./vendor/bin/phpunit
```

