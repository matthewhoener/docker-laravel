# Create a Docker Laravel Application

## Laravel Project
```bash
# Create new Laravel project
docker run --rm --interactive --tty \
  --volume $PWD:/app \
  --user $(id -u):$(id -g) \
  composer create-project --prefer-dist laravel/laravel projectname

# Rename project in composer.json
vim composer.json

# Initialize git repo
cd projectname
git init && git add --all && git commit -m 'Initial commit'
```

## Dev Docker Environment
Copy the [Dockerfile](./Dockerfile) to the new project.

Copy the [docker-compose.yml](./docker-compose.yml) to the new project and edit it appropriately.

Copy the [sample-readme.md](./sample-readme.md) to the new project and edit it appropriately.

Copy the [nginx](./nginx) directory to the new project.

## Set Up Tests
Edit `phpunit.xml` and add a `DB_DATABASE`.

```xml
<env name="DB_DATABASE" value="laravel_test"/>
```
